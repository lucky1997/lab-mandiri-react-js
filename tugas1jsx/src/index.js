import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const cita = "Kapiten"
const suara = "Hap"
const umur = 23

//Menggunakan JSX
const element_1 = (
  <div>
    <h4>Aku seorang {cita}, mempunyai Pedang Panjang.</h4>
    <br/>
    <h3>Kalau berjalan {suara+"-"+suara+"-"+suara}</h3>
    <br/>
    <h1>Tahun lahir {2020-umur}</h1>
  </div>
)

//menggunakan createElement()
const element_2 = React.createElement('h2',{className:'warna'}, 'Hai Semua, salam kenal')

//Atribut di JSX
const element_3 = <h2> {new Date().toTimeString()}</h2>

//Atribut Tanpa Child di JSX
const element_4 = <input type="text" placeholder="Username"/>

//Atribut Child di JSX
const element_5 = (
  <div>
    <ul>
      <li>Satu</li>
      <li>Dua</li>
      <li>Tiga</li>
      <li>Empat</li>
    </ul>
  </div>
)

ReactDOM.render(
  element_5,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
