import React from 'react'
import './Tombol.css'

class Tombol extends React.Component {
    render() {
        return(
            <div>
                <button type="submit" value="Submit">{this.props.tulisan}</button>
            </div>
        )
    }
}

export default Tombol