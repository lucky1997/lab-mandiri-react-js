import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import * as serviceWorker from './serviceWorker';
import Formulir from './components/Formulir/Formulir';
import Logo from './components/Logo/Logo';
import Tombol from './components/Tombol/Tombol';
import Judul from './components/Judul/Judul';
import gambar1 from './assets/images/logo.svg'

ReactDOM.render(
  <React.StrictMode>
    <Logo gambar={gambar1} />
    <Judul judul1="Selamat Datang di aplikasi React"/>
    <Formulir nama="Username"/>
    <Formulir nama="Password"/>
    <Tombol tulisan="Login" />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
